package main

import (
    "os"
    "fmt"
    "log"
    "strings"
    "bufio"
    "net"
    "simple-server/queue"
)

func main() {
    if len(os.Args) < 2 {
        cmd_invalid_usage()
    }

    data := parse_args(os.Args)

    listener, err := net.Listen("tcp", data.addr + ":" + data.port)
    if err != nil {
        log.Fatal("[FATAL] Error creating a listener:", err)
        os.Exit(1)
    }
    log.Println("[INFO] Server listening on:", data.addr + ":" + data.port)
    defer listener.Close()

    queue := queue.NewQueue()
    
    running := true
    for running {
        connection, err := listener.Accept()
        if err != nil {
            log.Fatal("[FATAL] Error accepting a connetion:", err)
            os.Exit(1)

        }
        go handle_connection(connection, queue)
    }

    os.Exit(0)
}

func cmd_usage() {
    fmt.Print(" --help or -h: Print this message.\n",
              " --addr or -a: Ip that server will listen on. (leave to use localhost)\n",
              " --port or -p: Port that the server will listen on. (must be specified)\n",
    )
}

func cmd_invalid_usage() {
    fmt.Println("Error. Invalid arguments. Checkout the usage:")
    cmd_usage()
    os.Exit(1)
}

type ServerData struct {
    addr string
    port string
}

func parse_args(args []string) ServerData {
    var data ServerData
    
    for i, arg := range args {
        if cmp_args(arg, "--help", "-h") {
            fmt.Println("Usage:")
            cmd_usage()
            os.Exit(0)
        } else if cmp_args(arg, "--addr", "-a") {
            data.addr = args[i + 1]
        } else if cmp_args(arg, "--port", "-p") {
            data.port = args[i + 1]
        }
    }
    
    if len(strings.TrimSpace(data.port)) == 0 {
        cmd_invalid_usage()
    }

    return data
}

func cmp_args(arg, op1, op2 string) bool {
    return arg == op1 || arg == op2
}

func handle_connection(connection net.Conn, queue *queue.Queue) {
    log.Println("[INFO] Connection accepted")
    
    defer connection.Close()

    queue.AddThread()
    defer queue.RemoveThread()
    
    shutdown := false
    go read_client_data(connection, queue, &shutdown)

    var previus_counter uint64 = 0
    for !shutdown {
        current_counter := queue.Counter
        
        if !(previus_counter - 1 == current_counter) && (current_counter != 0) {
            message := queue.GetMessage()
            if message != nil {
                connection.Write([]byte(message.(string)))
            }
            previus_counter = current_counter
        }
    }
}

func read_client_data(connection net.Conn, queue *queue.Queue, shutdown *bool) {
    for !*shutdown {
        client_data, err := bufio.NewReader(connection).ReadString('\n')
        if err != nil {
            log.Println("[ERROR] Invalid data comming from user")
            connection.Write([]byte("Invalid request error"))
            *shutdown = true
            // break
        }
        queue.PushMessage(client_data)
    }
}
