package queue

import "container/list"
import "sync"

type Queue struct {
    queue *list.List 
    threads uint64
    Counter uint64
    lock sync.Mutex
}

func NewQueue() *Queue {
    queue := Queue {
        queue: list.New(),
        threads: 0,
        Counter: 0,
    }
    return &queue
}

func (self *Queue) GetMessage() any {
    self.lock.Lock()
    defer self.lock.Unlock()
  
    var content any
    if self.queue.Front() != nil {
        self.Counter -= 1
    
        content = self.queue.Front().Value
    }

    // NOTE/FIXME: if a thread gets removed for whatever reason this will
    // not know and may not destroy the last element. However it could be 
    // fixed by just cleaning that on the application level
    if self.Counter == 0 {
        if content != nil {
            self.queue.Remove(self.queue.Front())
            self.Counter = self.threads
        }
    }

    return content
}

func (self *Queue) AddThread() {
    self.lock.Lock()
    defer self.lock.Unlock()

    self.threads += 1
}

func (self *Queue) RemoveThread() {
    self.lock.Lock()
    defer self.lock.Unlock()
    
    self.threads -= 1
}

func (self *Queue) PushMessage(content any) {
    self.lock.Lock()
    defer self.lock.Unlock()

    if self.queue.Front() == nil {
        self.Counter = self.threads
    }
    self.queue.PushBack(content)
}
